﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace UDPServer
{
    class Program
    {
        private static bool _closeProgram = false;
        private static bool _alreadyListening = false;
        private static bool _stopListening = false;
        private static Regex _portRegex = new Regex(@"(?'IP'[\d]{1,3}\.[\d]{1,3}.[\d]{1,3}.[\d]{1,3})(?:\:(?'PORT'[\d]+)){0,1}\s+(?'MSG'.+)");

        private static IPEndPoint _remoteEndPoint = new IPEndPoint(IPAddress.Any, 11000);

        private static UdpClient _server = null;
        private static UdpClient _client = null;

        static void Main(string[] args)
        {
            while (!_closeProgram)
            {
                Console.Write("UDP-Comm >");

                var input = Console.ReadLine().Trim();
                if (string.IsNullOrWhiteSpace(input))
                {
                    continue;
                }
                
                var parts = input.Split(" \t=".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries);
                var cmd = parts[0].ToLower();
                var value = parts.Length > 1 ? parts[1] : string.Empty;

                HandleCommand(cmd, value);
            }
        }

        static void HandleCommand(string cmd, string value)
        {
            if (cmd == "exit")
            {
                _closeProgram = true;
            }
            else if (cmd == "ip")
            {
                SetIP(value);
            }
            else if (cmd == "send")
            {
                SendString(value);
            }
            else if (cmd == "listen")
            {
                StartListening(value);
            }
            else if (cmd == "stop-listen")
            {
                StopListening();
            }
            else
            {
                Console.WriteLine("\nERR: Unknown command '{0}'", cmd);
            }
        }

        static void SetIP(string ipAddressString)
        {
            try
            {
                var ipAddress = IPAddress.Parse(ipAddressString);
                _remoteEndPoint = new IPEndPoint(ipAddress, 11000);
                Console.WriteLine("\nClient for UDP server at {0} created.\n", ipAddressString);
            }
            catch (Exception ex)
            {
                //Console.WriteLine("\nERR: Socket Exception appeared. Please check the error code '{0}' at 'Windows Sockets Error Codes' on MSDN\n", ex.ErrorCode);
                Console.WriteLine(string.Format("Unable to parse the IP Address specified.\nError: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
        }

        static void SendString(string msg)
        {
            try
            {
                string ip = string.Empty, message = string.Empty;
                int port = 11000;
                var portMatches = _portRegex.Matches(msg);
                if (portMatches.Count > 0)
                {
                    ip = portMatches[0].Groups["IP"].Value;
                    port = int.Parse(portMatches[0].Groups["PORT"].Value);
                    message = portMatches[0].Groups["MSG"].Value + "\n\r";
                }
                else
                {
                    throw new InvalidOperationException("IP, PORT or MESSAGE doesn't match expected format (i.e., '192.168.0.1:3000 Some message').");
                }

                var bytes = Encoding.ASCII.GetBytes(message.ToCharArray());
                using (var client = new UdpClient(port))
                {
                    client.SendAsync(bytes, bytes.Length, new IPEndPoint(IPAddress.Parse(ip), port)).ContinueWith(t =>
                    {
                        var sent = t.Result;
                        Console.WriteLine(string.Format("{0} bytes sent successfully", sent));
                    }).Wait();
                }
            }
            catch (SocketException ex)
            {
                Console.WriteLine(string.Format("Unable to send datagram.\nError: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(string.Format("Unable to parse send command.\nError: {0}\nStack Trace:\n{1}", ex.Message, ex.StackTrace));
            }
            
        }

        static async void StartListening(string msg)
        {
            if (_alreadyListening)
            {
                Console.WriteLine(string.Format("Already listening at {0}", _server.Client.LocalEndPoint.ToString()));
                return;
            }

            var port = 11000;
            int.TryParse(msg, out port);

            if (_server == null)
            {
                _server = new UdpClient(port);
                _remoteEndPoint = new IPEndPoint(IPAddress.Any, port);
            }

            _alreadyListening = true;
            _stopListening = false;
            Console.WriteLine(string.Format("Started listening at port {0}", port));

            using (var buffer = new FileStream(string.Format("udplog-{0:yyyy-MM-dd_hh-mm-ss}.bin", DateTime.Now), FileMode.Create))
            {
                using (var writer = new StreamWriter(buffer))
                {
                    while (true)
                    {
                        if (_stopListening)
                        {
                            Console.WriteLine(string.Format("Listening UDP stopped."));
                            break;
                        }

                        if (_server.Available > 0)
                        {
                            var result = await _server.ReceiveAsync();
                            string payload = Encoding.ASCII.GetString(result.Buffer);
                            await writer.WriteAsync(payload);
                            Console.Write(payload);
                        }
                        else
                            Thread.Sleep(15);
                    }

                    _server.Close();
                    _server = null;
                    _alreadyListening = false;
                }
            }
        }

        static void StopListening()
        {
            _stopListening = true;
            Console.WriteLine(string.Format("Stop listening request accepted. Awaiting current RX to be completed..."));
        }
    }
}
